/**
 * Given a list of items implement a navigation through the keyboard, following these requirements:
 * - Navigate through the list with UP and RIGHT keys will focus the next item.
 * - Navigate through the list with DOWN and LEFT keys will focus the previous item.
 * - Only one item will be FOCUSED per time in the browser.
 *
 * Suggestion: you may to think in term of "indexes".
 * You may calculate the index and use it to select the item, then you will focus that item.
 *
 * NOTE: The keydown event will work once the <ul> receives the focus.
 */

import { useEffect, useRef, useState } from "react";

const itemsList = Array(10).fill({
  item: "this is a navigation item",
});

export function ListItemsForNavigation(props) {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const activeItemRef = useRef();

  useEffect(
    function () {
      activeItemRef.current.focus();
    },
    [selectedIndex]
  );

  function handleKeyDown(event) {
    if (event.key === "ArrowDown" || event.key === "ArrowRight") {
      setSelectedIndex(Math.min(selectedIndex + 1, itemsList.length - 1));
    } else if (event.key === "ArrowUp" || event.key === "ArrowLeft") {
      setSelectedIndex(Math.max(selectedIndex - 1, 0));
    }
  }

  return (
    <ul
      tabIndex={0}
      onKeyDown={handleKeyDown}
      style={{ listStyleType: "none" }}
    >
      {itemsList.map((item, i) => (
        <li
          key={i}
          ref={selectedIndex === i ? activeItemRef : null}
          style={selectedIndex === i ? { color: "blue" } : null}
        >
          {item.item}
        </li>
      ))}
    </ul>
  );
}
